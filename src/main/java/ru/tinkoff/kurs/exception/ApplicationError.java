package ru.tinkoff.kurs.exception;

import com.fasterxml.jackson.annotation.JsonIgnore;


public enum ApplicationError {
    PROJECT_DOES_NOT_EXIST("Provided project does not exist!", 400),
    PERSON_ALREADY_EXISTS("Account card already exists!", 400),
    PROJECT_ALREADY_EXISTS("Project already exists!", 400),
    GIFT_ALREADY_EXISTS("Gift already exists!", 400),
    PERSON_NOT_FOUND("Account card not found!", 404),
    PROJECT_NOT_FOUND("Project not found!", 404),
    GIFT_NOT_FOUND("Gift not found!", 404),
    INVALID_DATE("Invalid date format! Please use DD-MM-YYYY format", 400),
    INVALID_ENTITY("Provided entity is noty valid!", 400),
    PASSWORDS_DO_NOT_MATCH("Passwords don't match!", 400),
    LOGIN_ALREADY_TAKEN("This login has been already taken!", 400),
    ACCOUNT_DOES_NOT_EXIST("Account does not exist!", 400),
    UNABLE_TO_CONNECT_DB("Unable to connect with database",500),
    UNABLE_TO_SEND_TO_TOPIC("Unable to send to topic using kafka", 500);
    private final String message;
    private final int code;

    ApplicationError(String message, int code) {
        this.message = message;
        this.code = code;
    }

    public ApplicationException exception(){
        return new ApplicationException(this);
    }

    public ApplicationException exception(String args){
        return new ApplicationException(this, args);
    }

    public static class ApplicationException extends RuntimeException{
        public final ApplicationExceptionCompanion applicationExceptionCompanion;

        ApplicationException(ApplicationError error){
            this.applicationExceptionCompanion = new ApplicationExceptionCompanion(error.code, error.message);
        }

        ApplicationException(ApplicationError error, String message){
            super(error.message+" : "+ message);
            this.applicationExceptionCompanion = new ApplicationExceptionCompanion(error.code, error.message+" : "+ message);
        }

        public static record ApplicationExceptionCompanion(@JsonIgnore int code, String message){

        }
    }




}
