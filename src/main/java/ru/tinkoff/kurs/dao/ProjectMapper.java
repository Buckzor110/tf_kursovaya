package ru.tinkoff.kurs.dao;

import org.apache.ibatis.annotations.Mapper;
import ru.tinkoff.kurs.model.Project;

import java.util.List;

@Mapper
public interface ProjectMapper {

    void saveProject(Project project);

    void deleteProject(String id);

    void updateProject(Project project);

    Project findById(String id);

    List<Project> findAllProjects();
}
