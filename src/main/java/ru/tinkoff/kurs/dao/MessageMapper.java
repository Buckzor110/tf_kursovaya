package ru.tinkoff.kurs.dao;

import org.apache.ibatis.annotations.Mapper;
import ru.tinkoff.kurs.model.KafkaMessage;

import java.util.List;

@Mapper
public interface MessageMapper {

     List<KafkaMessage> getMessage();

     void updateMessageState(String id);

     void saveMessage(KafkaMessage message);
}
