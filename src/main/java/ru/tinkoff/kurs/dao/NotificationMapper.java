package ru.tinkoff.kurs.dao;

import org.apache.ibatis.annotations.Mapper;
import ru.tinkoff.kurs.model.Notification;

import java.time.LocalDate;
import java.util.List;

@Mapper
public interface NotificationMapper {
    void saveNotification(String id, String type, String person, LocalDate person_date, String person_project, String person_city);

    void changeCurrentProject(String person, String person_project);

    void deleteNotification(String id);

    Notification findById(String id);

    List<Notification> findPersonNotifications(String id, String person_project, String person_city);

    List<Notification> findAllNotifications();

    void deleteExpiredNotifications();

}
