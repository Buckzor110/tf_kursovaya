package ru.tinkoff.kurs.dao;

import org.apache.ibatis.annotations.Mapper;
import ru.tinkoff.kurs.model.Gift;
import ru.tinkoff.kurs.model.Person;
import ru.tinkoff.kurs.model.PersonDTO;

import java.util.ArrayList;
import java.util.List;

@Mapper
public interface PersonMapper {

    void savePerson(Person person);

    void updatePerson(Person person);

    void deletePerson(String id);

    Person findById(String id);

    List<Person> findImeninniki();

    List<Person> getMyColleagues(String project, String id);

    List<Gift> findWishes(String person_id);

    void registerAccount(String id, String login, String password, ArrayList<String> roles);

    PersonDTO getAccountInfo(String login);

    String getAccountId(String login);

    void changeProject(String project_id, String person_id);

    void addGift(String person_id, String gift_id);

    void deleteGift(String person_id, String gift_id);
}
