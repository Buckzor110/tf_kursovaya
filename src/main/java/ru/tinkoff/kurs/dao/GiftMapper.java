package ru.tinkoff.kurs.dao;

import org.apache.ibatis.annotations.Mapper;
import ru.tinkoff.kurs.model.Gift;

import java.util.List;

@Mapper
public interface GiftMapper {
    void saveGift(Gift gift);

    void deleteGift(String id);

    Gift findById(String id);

    void updateGift(Gift gift);

    List<Gift> getAllGifts();

    List<Gift> findPersonWishlist(String person_id);
}
