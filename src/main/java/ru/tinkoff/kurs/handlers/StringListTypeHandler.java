package ru.tinkoff.kurs.handlers;

import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;

public class StringListTypeHandler extends BaseTypeHandler<ArrayList<String>> {
    @Override
    public void setNonNullParameter(PreparedStatement preparedStatement, int i, ArrayList<String> strings, JdbcType jdbcType) throws SQLException {
        StringBuilder str=new StringBuilder(strings.toString());
        preparedStatement.setString(i,str.substring(1,str.length()-1));
    }

    @Override
    public ArrayList<String> getNullableResult(ResultSet resultSet, String s) throws SQLException {
        String str = resultSet.getString(s);
        ArrayList<String> roles= new ArrayList<>();
        String[] rolesarray=str.split(", ");
        Collections.addAll(roles, rolesarray);
        return roles;
    }

    @Override
    public ArrayList<String> getNullableResult(ResultSet resultSet, int i) throws SQLException {
        String str = resultSet.getString(i);
        ArrayList<String> roles= new ArrayList<>();
        String[] rolesarray=str.split(", ");
        Collections.addAll(roles, rolesarray);
        return roles;
    }

    @Override
    public ArrayList<String> getNullableResult(CallableStatement callableStatement, int i) {
        return null;
    }
}
