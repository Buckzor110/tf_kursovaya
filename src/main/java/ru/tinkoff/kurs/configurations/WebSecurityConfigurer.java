package ru.tinkoff.kurs.configurations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.header.writers.StaticHeadersWriter;
import ru.tinkoff.kurs.services.PersonService;


@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
public class WebSecurityConfigurer extends WebSecurityConfigurerAdapter {

    @Autowired
    private PersonService personService;


    @Autowired
    public void configureUsers(AuthenticationManagerBuilder auth, PasswordEncoder encoder) throws Exception {
        auth.inMemoryAuthentication()
                .withUser("logadm").password(encoder.encode("testadm"))
                .roles("USER", "ADMIN");
        auth.userDetailsService(personService).passwordEncoder(encoder);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable().authorizeRequests()
                .antMatchers("/person*").hasRole("USER")
                .antMatchers("/person/details").hasRole("USER")
                .antMatchers(HttpMethod.GET,"/notifications*").hasRole("USER")
                .antMatchers(HttpMethod.GET,"/gifts*", "/projects*").hasRole("USER")
                .antMatchers(HttpMethod.DELETE, "/projects*", "/notifications*", "/gifts*").hasRole("USER")
                .antMatchers(HttpMethod.POST,"/projects*", "/notifications*", "/gifts*").hasRole("USER")
                .antMatchers(HttpMethod.PATCH,"/projects*", "/notifications*", "/gifts*" ).hasRole("USER")
                .antMatchers("/registration*").permitAll()
                .and()
                .headers()
                .addHeaderWriter(new StaticHeadersWriter("Access-Control-Allow-Origin", "*"))
                .addHeaderWriter(new StaticHeadersWriter("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE"))
                .addHeaderWriter(new StaticHeadersWriter("Access-Control-Allow-Headers", "Content-Type"))
                .and().httpBasic();
    }
}
