package ru.tinkoff.kurs.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tinkoff.kurs.cache.GiftCache;
import ru.tinkoff.kurs.model.Gift;
import ru.tinkoff.kurs.model.KafkaMessage;
import ru.tinkoff.kurs.dao.GiftMapper;

import java.util.List;
import java.util.UUID;

import static java.util.Objects.nonNull;
import static ru.tinkoff.kurs.exception.ApplicationError.GIFT_NOT_FOUND;

@Service
@Transactional
public class GiftService {

    @Autowired
    private GiftCache giftCache;

    @Autowired
    private GiftMapper mapper;

    public synchronized void saveGift(Gift gift){
        mapper.saveGift(gift);
        giftCache.addGift(gift);
    }

    public synchronized void deleteGift(String id){
        mapper.deleteGift(id);
        giftCache.removeGift(id);
    }

    public synchronized void updateGift(Gift gift){
        mapper.updateGift(gift);
        giftCache.removeGift(gift.getId());
    }

    public Gift findGift(String id){
        return giftCache.getGiftCache().computeIfAbsent(id, k -> {
            Gift gift = mapper.findById(id);
            if (nonNull(gift)){
                return gift;
            } else{
                throw GIFT_NOT_FOUND.exception();
            }
            });
    }

    public List<Gift> findPersonWishlist(String person_id){
        return mapper.findPersonWishlist(person_id);
    }

    public List<Gift> getAllGifts() {
        return mapper.getAllGifts();
    }
}
