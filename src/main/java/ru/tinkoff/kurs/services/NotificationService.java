package ru.tinkoff.kurs.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tinkoff.kurs.model.Notification;
import ru.tinkoff.kurs.model.Person;
import ru.tinkoff.kurs.dao.NotificationMapper;
import ru.tinkoff.kurs.dao.PersonMapper;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

import static java.util.Objects.nonNull;

@Service
@Transactional
public class NotificationService {

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private NotificationMapper mapper;

    @Autowired
    private PersonMapper personMapper;


    @Scheduled(cron = "0 0 0 * * *")
    public void updateNotificationTable(){
        mapper.deleteExpiredNotifications();
        List<Person> imeninniki = personMapper.findImeninniki();
        if (nonNull(imeninniki)){
            for (Person person : imeninniki){
                saveNotification(person);
            }
        }
    }


    public List<Notification> findPersonNotifications(String id){
        Person person = personMapper.findById(id);
        return mapper.findPersonNotifications(id, person.getProject(), person.getCity());
    }

    public List<Notification> findAllNotifications(){
        return mapper.findAllNotifications();
    }


    public void saveNotification(Person person) {
        if (person.getEmployment().getDayOfYear() - 7 <= LocalDate.now().getDayOfYear() && person.getEmployment().getDayOfYear() > LocalDate.now().getDayOfYear()){
            mapper.saveNotification(UUID.randomUUID().toString(), "employment", person.getId(), person.getEmployment(), person.getProject(), person.getCity());
        }
        if (person.getBirthday().getDayOfYear() - 7 <= LocalDate.now().getDayOfYear() && person.getBirthday().getDayOfYear() > LocalDate.now().getDayOfYear()){
            mapper.saveNotification(UUID.randomUUID().toString(), "birthday", person.getId(), person.getBirthday(), person.getProject(), person.getCity());
        }
    }


    public void deleteNotification(String id){
        mapper.deleteNotification(id);
    }

    public Notification findNotification(String id){
        return mapper.findById(id);
    }

}

