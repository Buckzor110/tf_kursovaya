package ru.tinkoff.kurs.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tinkoff.kurs.cache.ProjectCache;
import ru.tinkoff.kurs.model.KafkaMessage;
import ru.tinkoff.kurs.model.Project;
import ru.tinkoff.kurs.dao.ProjectMapper;

import java.util.List;
import java.util.UUID;

import static java.util.Objects.nonNull;
import static ru.tinkoff.kurs.exception.ApplicationError.PROJECT_NOT_FOUND;

@Service
@Transactional
public class ProjectService {

    @Autowired
    private ProjectMapper mapper;

    @Autowired
    private ProjectCache cache;

    public synchronized void saveProject(Project project){
        mapper.saveProject(project);
        cache.addProject(project);
    }

    public synchronized void deleteProject(String id){
        mapper.deleteProject(id);
        cache.removeProject(id);
    }

    public synchronized void updateProject(Project project){
        mapper.updateProject(project);
        cache.removeProject(project.getId());
    }

    public Project findProject(String id){
        return cache.getProjectCache().computeIfAbsent(id, k -> {
            Project project = mapper.findById(id);
            if (nonNull(project)){
                return project;
            } else{
                throw PROJECT_NOT_FOUND.exception();
            }
        });
    }

    public List<Project> findAllProjects(){
        return mapper.findAllProjects();
    }
}
