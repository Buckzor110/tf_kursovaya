package ru.tinkoff.kurs.services;


import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tinkoff.kurs.cache.PersonCache;
import ru.tinkoff.kurs.model.Gift;
import ru.tinkoff.kurs.model.Person;
import ru.tinkoff.kurs.model.PersonDTO;
import ru.tinkoff.kurs.dao.NotificationMapper;
import ru.tinkoff.kurs.dao.PersonMapper;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static java.util.Objects.nonNull;

@Service
@Transactional
public class PersonService implements UserDetailsService {

    @Autowired
    private PersonMapper mapper;

    @Autowired
    private PersonCache cache;

    @Autowired
    private PasswordEncoder encoder;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private NotificationMapper notificationMapper;

    public synchronized void deletePerson(String id){
        mapper.deletePerson(id);
        cache.removePerson(id);
    }

    public Person findById(String id){
        return cache.getPersonCache().computeIfAbsent(id, k ->{
            Person person = mapper.findById(id);
            person.setWishes(mapper.findWishes(person.getId()));
            return person;});
    }

    public synchronized void addGift(String person_id, String gift_id){
        mapper.addGift(person_id,gift_id);
        cache.removePerson(person_id);
    }

    public synchronized void changeProject(String project_id, String person_id){
        mapper.changeProject(project_id, person_id);
        notificationMapper.changeCurrentProject(person_id, project_id);
        cache.removePerson(person_id);
    }

    public synchronized void deleteGift(String person_id, String gift_id){
        mapper.deleteGift(person_id,gift_id);
        cache.removePerson(person_id);
    }

    public List<Person> findImeninniki(){
        List<Person> imeninniki = mapper.findImeninniki();
        for (Person person: imeninniki){
            person.setWishes(mapper.findWishes(person.getId()));
        }
        return imeninniki;
    }

    public List<Person> getMyColleagues(String id){
        Person person = findById(id);
        return mapper.getMyColleagues(person.getProject(), person.getId());
    }

    public List<Gift> findWishes(String personId){
        return mapper.findWishes(personId);
    }

    public String getAccountId(String login){
        return mapper.getAccountId(login);
    }

    public void SaveUserConfig(String login, PersonDTO params){
        PersonDTO personDTO = mapper.getAccountInfo(login);
        Person person = new Person();
        person.setId(personDTO.getId());
        person.setName(params.getName());
        person.setPhoneNumber(params.getPhoneNumber());
        person.setProject(params.getProject());
        person.setBirthday(params.getBirthday());
        person.setEmployment(params.getEmployment());
        person.setCity(params.getCity());
        mapper.savePerson(person);
        cache.addPerson(person);
    }

    public synchronized void updatePerson(String login, PersonDTO paramsToUpdate){
        PersonDTO info = mapper.getAccountInfo(login);
        Person person = mapper.findById(info.getId());
        if (nonNull(paramsToUpdate.getName())){
            person.setName(paramsToUpdate.getName());
        }
        if (nonNull(paramsToUpdate.getCity())){
            person.setCity(paramsToUpdate.getCity());
        }
        if (nonNull(paramsToUpdate.getPhoneNumber())){
            person.setPhoneNumber(paramsToUpdate.getPhoneNumber());
        }
        mapper.updatePerson(person);
        cache.removePerson(person.getId());
    }

    public UserDetails loadUserByUsername(String username){
        PersonDTO personDTO = mapper.getAccountInfo(username);
        return new org.springframework.security.core.userdetails.User(
                personDTO.getLogin(), personDTO.getPassword(), getAuthorities(personDTO.getRoles()));
    }

    private static List<GrantedAuthority> getAuthorities (ArrayList<String> roles) {
        List<GrantedAuthority> authorities = new ArrayList<>();
        for(String role: roles){
            authorities.add(new SimpleGrantedAuthority(role));
        }
        return authorities;
    }

    public void registerPerson(PersonDTO personDTO){
        mapper.registerAccount(UUID.randomUUID().toString(), personDTO.getLogin(), encoder.encode(personDTO.getPassword()), personDTO.getRoles());
    }
}
