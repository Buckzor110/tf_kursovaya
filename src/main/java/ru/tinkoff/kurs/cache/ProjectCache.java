package ru.tinkoff.kurs.cache;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import ru.tinkoff.kurs.model.Project;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Slf4j
@Component
public class ProjectCache {
    private final Map<String, Project> projectCache = new ConcurrentHashMap<>();
    public void addProject(Project project){
        projectCache.put(project.getId(), project);
        log.info("Project cache successfully added");
    }

    public Project findProject(String id){
        return projectCache.get(id);
    }

    public Map<String, Project> getProjectCache() {
        return projectCache;
    }

    public void removeProject(String id){
        projectCache.remove(id);
        log.info("Project cache successfully removed");
    }
}
