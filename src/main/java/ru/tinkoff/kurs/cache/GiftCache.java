package ru.tinkoff.kurs.cache;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import ru.tinkoff.kurs.model.Gift;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Slf4j
@Component
public class GiftCache {
    private final Map<String, Gift> giftCache = new ConcurrentHashMap<>();

    public void addGift(Gift gift){
        giftCache.put(gift.getId(), gift);
        log.info("Gift cache successfully added");
    }

    public Gift findGift(String id){
        return giftCache.get(id);
    }

    public Map<String, Gift> getGiftCache() {
        return giftCache;
    }

    public void removeGift(String id){
        giftCache.remove(id);
        log.info("Gift cache successfully removed");
    }
}
