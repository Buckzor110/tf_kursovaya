package ru.tinkoff.kurs.cache;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import ru.tinkoff.kurs.model.Person;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Slf4j
@Component
public class PersonCache {
    private final Map<String, Person> personCache = new ConcurrentHashMap<>();

    public void addPerson(Person person){
        personCache.put(person.getId(), person);
        log.info("Person cache successfully added");
    }

    public Person findPerson(String id){
        return personCache.get(id);
    }

    public Map<String, Person> getPersonCache() {
        return personCache;
    }

    public void removePerson(String id){
        personCache.remove(id);
        log.info("Person cache successfully removed");
    }
}
