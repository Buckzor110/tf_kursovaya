package ru.tinkoff.kurs.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

import java.time.LocalDate;
import java.util.ArrayList;

@Data
@NoArgsConstructor
@AllArgsConstructor
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class PersonDTO {
    String id;
    @NonNull String name;
    @NonNull String phoneNumber;
    @NonNull String project;
    @NonNull String city;
    @JsonFormat(pattern = "dd-MM-yyyy")
    @NonNull LocalDate employment;

    @JsonFormat(pattern = "dd-MM-yyyy")
    @NonNull LocalDate birthday;

    ArrayList<String> wishes;
    ArrayList<String> roles;
    String login;
    String password;
    String matchingPassword;

    public PersonDTO(String login, String password, String matchingPassword){
        this.login = login;
        this.password = password;
        this.matchingPassword = matchingPassword;
    }
}
