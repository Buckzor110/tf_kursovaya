package ru.tinkoff.kurs.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class KafkaMessage {
    String messageId;
    String id;
    String type;
    String state;
}
