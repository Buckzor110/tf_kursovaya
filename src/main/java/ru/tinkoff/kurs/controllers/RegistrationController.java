package ru.tinkoff.kurs.controllers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.tinkoff.kurs.model.PersonDTO;
import ru.tinkoff.kurs.exception.ApplicationError;
import ru.tinkoff.kurs.services.PersonService;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static ru.tinkoff.kurs.exception.ApplicationError.LOGIN_ALREADY_TAKEN;
import static ru.tinkoff.kurs.exception.ApplicationError.PASSWORDS_DO_NOT_MATCH;

@RestController
@RequestMapping(path = "/registration")
@Slf4j
@CrossOrigin
public class RegistrationController {

    @Autowired
    private PersonService service;

    @PostMapping(consumes = APPLICATION_JSON_VALUE)
    public void registerPerson(@RequestBody PersonDTO personDTO){
        log.info(personDTO.toString());
        if (!personDTO.getPassword().equals(personDTO.getMatchingPassword())){
            throw PASSWORDS_DO_NOT_MATCH.exception();
        }
        try{
            if (personDTO.getRoles() == null){
                personDTO.setRoles(new ArrayList<>(List.of("USER")));
            }
            service.registerPerson(personDTO);
        } catch(DuplicateKeyException e){
            throw LOGIN_ALREADY_TAKEN.exception();
        }

        log.info("New person with login {} has been registered!", personDTO.getLogin());
    }

    @ExceptionHandler({ApplicationError.ApplicationException.class})
    public ResponseEntity<ApplicationError.ApplicationException.ApplicationExceptionCompanion> handleApplicationException(ApplicationError.ApplicationException e){
        return ResponseEntity.status(e.applicationExceptionCompanion.code()).body(e.applicationExceptionCompanion);
    }
}
