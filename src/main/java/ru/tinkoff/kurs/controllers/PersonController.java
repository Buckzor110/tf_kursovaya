package ru.tinkoff.kurs.controllers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import ru.tinkoff.kurs.model.Gift;
import ru.tinkoff.kurs.model.Notification;
import ru.tinkoff.kurs.model.Person;
import ru.tinkoff.kurs.model.PersonDTO;
import ru.tinkoff.kurs.exception.ApplicationError;
import ru.tinkoff.kurs.services.GiftService;
import ru.tinkoff.kurs.services.NotificationService;
import ru.tinkoff.kurs.services.PersonService;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static ru.tinkoff.kurs.exception.ApplicationError.*;

@RestController
@RequestMapping(path = "/person/**")
@Slf4j
@CrossOrigin
public class PersonController {

    @Autowired
    private PersonService service;

    @Autowired
    private GiftService giftService;

    @Autowired
    private NotificationService notificationService;

    private String getCurrentPersonId(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String login = authentication.getName();
        return service.getAccountId(login);
    }

    @GetMapping(produces = APPLICATION_JSON_VALUE, path = "/person/details")
    public UserDetails getCredentials(@RequestParam("login")String login){
        try{
            return service.loadUserByUsername(login);
        }
        catch (NullPointerException e){
            throw ACCOUNT_DOES_NOT_EXIST.exception();
        }
    }

    @PostMapping(path = "/person/project/change")
    public void changeProject(@RequestParam("project_id") String project_id){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String login = authentication.getName();
        if (service.getAccountId(login) == null){
            throw ACCOUNT_DOES_NOT_EXIST.exception();
        }
        try{ service.findById(service.getAccountId(login));}
        catch(NullPointerException e){
            throw PERSON_NOT_FOUND.exception();
        }
        try{ service.changeProject(project_id, service.getAccountId(login));}
        catch (DataIntegrityViolationException e){
           throw PROJECT_DOES_NOT_EXIST.exception();
       }
        log.info("Your current project has been switched to project with id {}!", project_id);
    }


    @PostMapping(consumes = APPLICATION_JSON_VALUE, path = "/person/info/save")
    public void savePersonData(@RequestBody PersonDTO personDTO){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String login = authentication.getName();
        if (personDTO.getName() == null  || personDTO.getPhoneNumber() == null || personDTO.getCity() == null|| personDTO.getName().isBlank() || personDTO.getCity().isBlank() || personDTO.getPhoneNumber().isBlank()) {
            throw INVALID_ENTITY.exception();
        }
        try{
            service.SaveUserConfig(login, personDTO);
        }
        catch(NullPointerException e){
            throw ACCOUNT_DOES_NOT_EXIST.exception();
        }
        catch (HttpMessageNotReadableException e){
            throw INVALID_DATE.exception();
        }
        catch (DuplicateKeyException e){
            throw PERSON_ALREADY_EXISTS.exception();
        }
        catch (DataIntegrityViolationException e){
            throw PROJECT_DOES_NOT_EXIST.exception();
        }
        log.info("New person with name {} has been registered!", personDTO.getName());
    }

    @PostMapping(path = "/person/my-wishes/create", consumes = APPLICATION_JSON_VALUE)
    public void addGiftToPerson(@RequestBody Gift gift){
        String id = getCurrentPersonId();
        if (id == null){
            throw ACCOUNT_DOES_NOT_EXIST.exception();
        }
        if (gift.getName() == null || gift.getId() == null || gift.getDescription() == null || gift.getId().isBlank() || gift.getName().isBlank() || gift.getDescription().isBlank()) {
            throw INVALID_ENTITY.exception();
        }
        try{
            giftService.saveGift(gift);
        }
        catch (DuplicateKeyException e){
            throw GIFT_ALREADY_EXISTS.exception();
        }
        service.addGift(id,gift.getId());
        log.info("Gift {} has been created and added to your account!", gift.getName());
    }

    @PostMapping(path = "/person/my-wishes/add")
    public void addGiftToPerson(@RequestParam("gift_id") String gift_id){
        String id = getCurrentPersonId();
        if (id == null){
            throw ACCOUNT_DOES_NOT_EXIST.exception();
        }

        try{giftService.findGift(gift_id);}
        catch(NullPointerException e){
            throw GIFT_NOT_FOUND.exception(gift_id);
        }
        service.addGift(id,gift_id);
        log.info("Gift with id {} has been added to your card!", gift_id);
    }

    @DeleteMapping(path = "/person/my-wishes/delete")
    public void deleteGiftFromPerson(@RequestParam("gift_id") String gift_id){
        String id = getCurrentPersonId();
        if (id == null){
            throw ACCOUNT_DOES_NOT_EXIST.exception();
        }
        try{giftService.findGift(gift_id);}
        catch(NullPointerException e){
            throw GIFT_NOT_FOUND.exception(gift_id);
        }
        service.deleteGift(id,gift_id);
        log.info("Gift with id {} has been deleted from your card!", gift_id);
    }

    @GetMapping(path = "/person/my-wishes", produces = APPLICATION_JSON_VALUE)
    public List<Gift> showPersonGifts(){
        String id = getCurrentPersonId();
        if (id == null){
            throw ACCOUNT_DOES_NOT_EXIST.exception();
        }
        return service.findWishes(id);
    }

    @GetMapping(path = "/person/my-notifications", produces = APPLICATION_JSON_VALUE)
    public List<Notification> showPersonNotifications(){
        String id = getCurrentPersonId();
        if (id == null){
            throw ACCOUNT_DOES_NOT_EXIST.exception();
        }

        return notificationService.findPersonNotifications(id);
    }

    @GetMapping(produces = APPLICATION_JSON_VALUE)
    public Person findPerson(@RequestParam("id") String id){
        try{
            service.findById(id);
        } catch(NullPointerException e){
            throw PERSON_NOT_FOUND.exception(id);
        }
        return service.findById(id);
    }

    @DeleteMapping(path = "/person/delete")
    public void deletePersonAccount(){
        String id = getCurrentPersonId();
        if (id == null){
            throw ACCOUNT_DOES_NOT_EXIST.exception();
        }
        service.deletePerson(id);
        log.info("Person with id {} has been deleted!", id);
    }

    @GetMapping(path = "/person/my-colleagues", produces = APPLICATION_JSON_VALUE)
    public List<Person> getMyColleagues(){
        String id = getCurrentPersonId();
        if (id == null){
            throw ACCOUNT_DOES_NOT_EXIST.exception();
        }
        return service.getMyColleagues(id);
    }

    @GetMapping(path = "/person/info", produces = APPLICATION_JSON_VALUE)
    public Person updatePerson(){
        String id = getCurrentPersonId();
        Person person = service.findById(id);
        if (person == null){
            throw PERSON_NOT_FOUND.exception();
        }
        return person;
    }

    @PatchMapping(path = "/person/info/update", consumes = APPLICATION_JSON_VALUE)
    public void updatePerson(@RequestBody PersonDTO person){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String login = authentication.getName();
        String id = getCurrentPersonId();
        if (id == null){
            throw ACCOUNT_DOES_NOT_EXIST.exception();
        }
        try{
            service.findById(id);
        } catch (Exception e){
            throw PERSON_NOT_FOUND.exception();
        }
        service.updatePerson(login, person);
        log.info("Your card has been updated!");
    }

    @ExceptionHandler({ApplicationError.ApplicationException.class})
    public ResponseEntity<ApplicationError.ApplicationException.ApplicationExceptionCompanion> handleApplicationException(ApplicationError.ApplicationException e){
        return ResponseEntity.status(e.applicationExceptionCompanion.code()).body(e.applicationExceptionCompanion);
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ResponseEntity<Object> handleException() {
        return ResponseEntity.status(INVALID_DATE.exception().applicationExceptionCompanion.code()).body(INVALID_DATE.exception().applicationExceptionCompanion);
    }
}
