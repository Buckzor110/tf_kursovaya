package ru.tinkoff.kurs.controllers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.tinkoff.kurs.model.Project;
import ru.tinkoff.kurs.exception.ApplicationError;
import ru.tinkoff.kurs.services.ProjectService;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static ru.tinkoff.kurs.exception.ApplicationError.*;

@RestController
@RequestMapping(path = "/projects/*")
@Slf4j
@CrossOrigin
public class ProjectController {
    @Autowired
    private ProjectService service;

    @PostMapping(consumes = APPLICATION_JSON_VALUE, path = "/projects/create")
    public void saveProject(@RequestBody Project project){
        if (project.getName() == null || project.getId() == null || project.getDescription() == null || project.getId().isBlank() || project.getName().isBlank() || project.getDescription().isBlank()) {
            throw INVALID_ENTITY.exception();
        }
        try{
            service.saveProject(project);
        } catch (DuplicateKeyException e){
            throw PROJECT_ALREADY_EXISTS.exception();
        }
        log.info("New project with name {} has been added!", project.getName());
    }

    @GetMapping(produces = APPLICATION_JSON_VALUE)
    public Project findProject(@RequestParam("id") String id){
        try{
            service.findProject(id);
        } catch(NullPointerException e){
            throw PROJECT_NOT_FOUND.exception(id);
        }
        return service.findProject(id);
    }

    @GetMapping(produces = APPLICATION_JSON_VALUE, path = "/projects/all")
    public List<Project> findAllProjects(){
        return service.findAllProjects();
    }

    @DeleteMapping(path = "/projects/delete")
    public void deleteProject(@RequestParam("id") String id){
        if (service.findProject(id)==null){
            throw PROJECT_NOT_FOUND.exception();
        }
        service.deleteProject(id);
        log.info("Project with id {} has been deleted!", id);
    }

    @PatchMapping(path = "/projects/update")
    public void updateProject(@RequestBody Project project){
        try{
            service.findProject(project.getId());
        } catch (Exception e){
            throw PROJECT_NOT_FOUND.exception();
        }
        service.updateProject(project);
        log.info("Project with name {} has been updated!", project.getName());
    }

    @ExceptionHandler({ApplicationError.ApplicationException.class})
    public ResponseEntity<ApplicationException.ApplicationExceptionCompanion> handleApplicationException(ApplicationError.ApplicationException e){
        return ResponseEntity.status(e.applicationExceptionCompanion.code()).body(e.applicationExceptionCompanion);
    }

}
