package ru.tinkoff.kurs.controllers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.tinkoff.kurs.model.Gift;
import ru.tinkoff.kurs.exception.ApplicationError;
import ru.tinkoff.kurs.services.GiftService;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static ru.tinkoff.kurs.exception.ApplicationError.*;

@RestController
@RequestMapping(path = "/gifts/**")
@Slf4j
@CrossOrigin
public class GiftController {

    @Autowired
    private GiftService service;

    @PostMapping(consumes = APPLICATION_JSON_VALUE, path = "/gifts/create")
    public void saveGift(@RequestBody Gift gift){
        if (gift.getName() == null || gift.getId() == null || gift.getDescription() == null || gift.getId().isBlank() || gift.getName().isBlank() || gift.getDescription().isBlank()) {
            throw INVALID_ENTITY.exception();
        }
        try{
            service.saveGift(gift);
        }
        catch (DuplicateKeyException e){
            throw GIFT_ALREADY_EXISTS.exception();
        }
        log.info("New gift with name {} has been added!", gift.getName());
    }

    @GetMapping(produces = APPLICATION_JSON_VALUE, path = "/gifts/person")
    public List<Gift> findPersonWishlist(@RequestParam("person_id") String id){
        return service.findPersonWishlist(id);
    }

    @GetMapping(produces = APPLICATION_JSON_VALUE)
    public Gift findGift(@RequestParam("id") String id){
        try{
            service.findGift(id);
        } catch(NullPointerException e){
            throw GIFT_NOT_FOUND.exception(id);
        }
        return service.findGift(id);
    }

    @DeleteMapping(path = "/gifts/delete")
    public void deleteGift(@RequestParam("id") String id){
        try{
            service.findGift(id);
        } catch (Exception e){
            throw GIFT_NOT_FOUND.exception();
        }
        service.deleteGift(id);
        log.info("Gift with id {} has been deleted!", id);
    }

    @PatchMapping(path = "/gifts/update")
    public void updateGift(@RequestBody Gift gift){
        try{
            service.findGift(gift.getId());
        } catch (Exception e){
            throw GIFT_NOT_FOUND.exception();
        }
        service.updateGift(gift);
        log.info("Gift with id {} has been updated!", gift.getId());
    }

    @GetMapping(produces = APPLICATION_JSON_VALUE, path = "/gifts/all")
    public List<Gift> getAllGifts(){
        return service.getAllGifts();
    }


    @ExceptionHandler({ApplicationError.ApplicationException.class})
    public ResponseEntity<ApplicationException.ApplicationExceptionCompanion> handleApplicationException(ApplicationError.ApplicationException e){
        return ResponseEntity.status(e.applicationExceptionCompanion.code()).body(e.applicationExceptionCompanion);
    }
}
