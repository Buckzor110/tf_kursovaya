package ru.tinkoff.kurs.controllers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.tinkoff.kurs.model.Notification;
import ru.tinkoff.kurs.model.Person;
import ru.tinkoff.kurs.services.NotificationService;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(path = "/notifications/*")
@Slf4j
@CrossOrigin
public class NotificationController {

    @Autowired
    private NotificationService service;

    @PostMapping(consumes = APPLICATION_JSON_VALUE)
    public void saveNotification(@RequestBody Person person) {
        service.saveNotification(person);
        log.info("New notification for person with name {} has been added!", person.getName());
    }

    @GetMapping(produces = APPLICATION_JSON_VALUE, path = "/notifications/all")
    public List<Notification> getAllNotifications(){
        return service.findAllNotifications();
    }

    @GetMapping(produces = APPLICATION_JSON_VALUE)
    public Notification findNotification(@RequestParam("id") String id){
        return service.findNotification(id);
    }

    @GetMapping(produces = APPLICATION_JSON_VALUE, path = "/notifications/person")
    public List<Notification> findPersonNotifications(@RequestParam("id") String id){
        return service.findPersonNotifications(id);
    }

    @DeleteMapping(path = "/notifications/delete")
    public void deleteNotification(@RequestParam("id") String id){
        service.deleteNotification(id);
        log.info("Notification with id {} has been deleted!", id);
    }

    @GetMapping(path = "/notifications/refresh")
    public void updateNotifications() {
        service.updateNotificationTable();
    }

}
