CREATE TABLE outbox
(
    messageId   VARCHAR PRIMARY KEY,
    id   VARCHAR,
    type VARCHAR,
    state VARCHAR,
    isSent BOOLEAN
);