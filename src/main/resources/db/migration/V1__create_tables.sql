CREATE TABLE project
(
    id   VARCHAR,
    name VARCHAR NOT NULL,
    description VARCHAR NOT NULL,
    PRIMARY KEY(id)
);

CREATE TABLE person_login_password
(
    person_id VARCHAR NOT NULL,
    login VARCHAR NOT NULL UNIQUE,
    password VARCHAR NOT NULL,
    roles VARCHAR NOT NULL,
    PRIMARY KEY(person_id)

);

CREATE TABLE person
(
    id   VARCHAR NOT NULL,
    name VARCHAR NOT NULL,
    phoneNumber VARCHAR NOT NULL,
    project VARCHAR NOT NULL,
    city VARCHAR NOT NULL,
    birthday  DATE NOT NULL,
    employment DATE NOT NULL,
    PRIMARY KEY(id),
    FOREIGN KEY (project) REFERENCES project(id) ON DELETE SET NULL,
    FOREIGN KEY (id) REFERENCES person_login_password(person_id) ON DELETE CASCADE
);

CREATE TABLE gift
(
    id   VARCHAR,
    name VARCHAR NOT NULL,
    description VARCHAR NOT NULL,
    url VARCHAR,
    PRIMARY KEY(id)
);


CREATE TABLE notification
(
    id   VARCHAR,
    type VARCHAR NOT NULL,
    person VARCHAR  NOT NULL,
    person_date DATE NOT NULL,
    person_project VARCHAR NOT NULL,
    person_city VARCHAR NOT NULL,
    PRIMARY KEY(id),
    FOREIGN KEY (person) REFERENCES person(id) ON DELETE CASCADE
);
