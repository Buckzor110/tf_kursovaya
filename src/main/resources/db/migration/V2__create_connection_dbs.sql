CREATE TABLE person_to_gift
(
    person_id VARCHAR NOT NULL, FOREIGN KEY (person_id) REFERENCES person(id) ON DELETE CASCADE,
    gift_id VARCHAR NOT NULL, FOREIGN KEY (gift_id) REFERENCES gift(id) ON DELETE CASCADE
);
