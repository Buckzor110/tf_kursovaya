package ru.tinkoff.kurs.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors;
import org.springframework.test.web.servlet.MockMvc;
import ru.tinkoff.kurs.AbstractClass;
import ru.tinkoff.kurs.model.Gift;
import ru.tinkoff.kurs.model.PersonDTO;
import ru.tinkoff.kurs.services.PersonService;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser(username = "any", password = "thing", roles = {"USER","ADMIN"})
class NotificationControllerTest extends AbstractClass {

    @Autowired
    private PersonService personService;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper jackson;

    @BeforeEach
    public void setUp() throws Exception{
        String projectJSON = jackson.writeValueAsString(project);
        mockMvc.perform(post("/projects/create").contentType("application/json").content(projectJSON)).andExpect(status().isOk());
        for (Gift gift: gifts){
            mockMvc.perform(post("/gifts/create").contentType("application/json").content(jackson.writeValueAsString(gift))).andExpect(status().isOk());
        }
        mockMvc.perform(post("/registration").contentType("application/json").content(jackson.writeValueAsString(preparePerson1Login())));
        mockMvc.perform(post("/registration").contentType("application/json").content(jackson.writeValueAsString(preparePerson2Login())));
        mockMvc.perform(post("/person/info/save").contentType("application/json").content(jackson.writeValueAsString(preparePerson1Info())).with(SecurityMockMvcRequestPostProcessors.httpBasic("login", "password"))).andExpect(status().isOk());
        mockMvc.perform(post("/person/info/save").contentType("application/json").content(jackson.writeValueAsString(preparePerson2Info())).with(SecurityMockMvcRequestPostProcessors.httpBasic("anotherLogin", "password1"))).andExpect(status().isOk());
    }

    @Test
    void refreshNotifications() throws Exception {
        mockMvc.perform(get("/notifications/refresh").with(SecurityMockMvcRequestPostProcessors.httpBasic("logadm", "testadm"))).andExpect(status().isOk());
        System.out.println(mockMvc.perform(get("/notifications/all").with(SecurityMockMvcRequestPostProcessors.httpBasic("logadm", "testadm"))).andReturn().getResponse().getContentAsString());
    }


}