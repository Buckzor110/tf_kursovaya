package ru.tinkoff.kurs.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors;
import org.springframework.test.web.servlet.MockMvc;
import ru.tinkoff.kurs.AbstractClass;
import ru.tinkoff.kurs.model.Gift;
import ru.tinkoff.kurs.services.PersonService;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@WithMockUser(username = "any", password = "thing", roles = {"USER","ADMIN"})
class PersonControllerTest extends AbstractClass {

    @Autowired
    private PersonService personService;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper jackson;

    @BeforeEach
    public void setUp() throws Exception{
        String projectJSON = jackson.writeValueAsString(project);
        String anotherProjectJSON = jackson.writeValueAsString(anotherProject);
        mockMvc.perform(post("/projects/create").contentType("application/json").content(projectJSON)).andExpect(status().isOk());
        mockMvc.perform(post("/projects/create").contentType("application/json").content(anotherProjectJSON)).andExpect(status().isOk());
        for (Gift gift: gifts){
            mockMvc.perform(post("/gifts/create").contentType("application/json").content(jackson.writeValueAsString(gift))).andExpect(status().isOk());
        }
    }

    @Test
    void addInfoSuccess() throws Exception{
        mockMvc.perform(post("/registration").contentType("application/json").content(jackson.writeValueAsString(preparePerson1Login())));
        mockMvc.perform(post("/person/info/save").contentType("application/json").content(jackson.writeValueAsString(preparePerson1Info())).with(SecurityMockMvcRequestPostProcessors.httpBasic("login", "password"))).andExpect(status().isOk());
    }

    @Test
    void addGiftSuccess() throws Exception{
        mockMvc.perform(post("/registration").contentType("application/json").content(jackson.writeValueAsString(preparePerson1Login())));
        mockMvc.perform(post("/person/info/save").contentType("application/json").content(jackson.writeValueAsString(preparePerson1Info())).with(SecurityMockMvcRequestPostProcessors.httpBasic("login", "password"))).andExpect(status().isOk());
        mockMvc.perform(post("/person/my-wishes/add").param("gift_id", "49ab4b33-2a73-41ef-7ae3-10ea7856a3ed").with(SecurityMockMvcRequestPostProcessors.httpBasic("login", "password"))).andExpect(status().isOk());
    }

    @Test
    void getMyColleagues() throws Exception{
        mockMvc.perform(post("/registration").contentType("application/json").content(jackson.writeValueAsString(preparePerson1Login())));
        mockMvc.perform(post("/person/info/save").contentType("application/json").content(jackson.writeValueAsString(preparePerson1Info())).with(SecurityMockMvcRequestPostProcessors.httpBasic("login", "password"))).andExpect(status().isOk());
        mockMvc.perform(get("/person/my-colleagues").with(SecurityMockMvcRequestPostProcessors.httpBasic("login", "password"))).andExpect(status().isOk());
    }

    @Test
    void deleteGiftSuccess() throws Exception{
        mockMvc.perform(post("/registration").contentType("application/json").content(jackson.writeValueAsString(preparePerson1Login())));
        mockMvc.perform(post("/person/info/save").contentType("application/json").content(jackson.writeValueAsString(preparePerson1Info())).with(SecurityMockMvcRequestPostProcessors.httpBasic("login", "password"))).andExpect(status().isOk());
        mockMvc.perform(post("/person/my-wishes/add").param("gift_id", "49ab4b33-2a73-41ef-7ae3-10ea7856a3ed").with(SecurityMockMvcRequestPostProcessors.httpBasic("login", "password"))).andExpect(status().isOk());
        mockMvc.perform(delete("/person/my-wishes/delete").param("gift_id", "49ab4b33-2a73-41ef-7ae3-10ea7856a3ed").with(SecurityMockMvcRequestPostProcessors.httpBasic("login", "password"))).andExpect(status().isOk());
    }


    @Test
    void findMyWishes() throws Exception{
        mockMvc.perform(post("/registration").contentType("application/json").content(jackson.writeValueAsString(preparePerson1Login())));
        mockMvc.perform(post("/person/info/save").contentType("application/json").content(jackson.writeValueAsString(preparePerson1Info())).with(SecurityMockMvcRequestPostProcessors.httpBasic("login", "password"))).andExpect(status().isOk());
        mockMvc.perform(post("/person/my-wishes/add").param("gift_id", "49ab4b33-2a73-41ef-7ae3-10ea7856a3ed").with(SecurityMockMvcRequestPostProcessors.httpBasic("login", "password"))).andExpect(status().isOk());
        mockMvc.perform(get("/person/my-wishes").with(SecurityMockMvcRequestPostProcessors.httpBasic("login", "password"))).andExpect(status().isOk());
    }

    @Test
    void findMyNotifications() throws Exception{
        mockMvc.perform(post("/registration").contentType("application/json").content(jackson.writeValueAsString(preparePerson1Login())));
        mockMvc.perform(post("/person/info/save").contentType("application/json").content(jackson.writeValueAsString(preparePerson1Info())).with(SecurityMockMvcRequestPostProcessors.httpBasic("login", "password"))).andExpect(status().isOk());
        mockMvc.perform(get("/person/my-notifications").with(SecurityMockMvcRequestPostProcessors.httpBasic("login", "password"))).andExpect(status().isOk());
    }

    @Test
    @Order(1)
    void findPerson() throws Exception{
        mockMvc.perform(post("/registration").contentType("application/json").content(jackson.writeValueAsString(preparePerson1Login())));
        mockMvc.perform(post("/registration").contentType("application/json").content(jackson.writeValueAsString(preparePerson2Login())));
        mockMvc.perform(post("/person/info/save").contentType("application/json").content(jackson.writeValueAsString(preparePerson1Info())).with(SecurityMockMvcRequestPostProcessors.httpBasic("login", "password"))).andExpect(status().isOk());
        mockMvc.perform(post("/person/info/save").contentType("application/json").content(jackson.writeValueAsString(preparePerson2Info())).with(SecurityMockMvcRequestPostProcessors.httpBasic("anotherLogin", "password1"))).andExpect(status().isOk());
        mockMvc.perform(get("/person/").param("id", personService.getAccountId("anotherLogin")).with(SecurityMockMvcRequestPostProcessors.httpBasic("login", "password"))).andExpect(status().isOk());
    }

    @Test
    void personNotFound() throws Exception{
        mockMvc.perform(post("/registration").contentType("application/json").content(jackson.writeValueAsString(preparePerson1Login())));
        mockMvc.perform(post("/person/info/save").contentType("application/json").content(jackson.writeValueAsString(preparePerson1Info())).with(SecurityMockMvcRequestPostProcessors.httpBasic("login", "password"))).andExpect(status().isOk());
        mockMvc.perform(get("/person/").param("id","48ab4b33-2a73-41ef-7ae3-10ea7856a3ed" ).with(SecurityMockMvcRequestPostProcessors.httpBasic("login", "password"))).andExpect(status().is4xxClientError());
    }


    @Test
    void createGiftSuccess() throws Exception{
        mockMvc.perform(post("/registration").contentType("application/json").content(jackson.writeValueAsString(preparePerson1Login())));
        mockMvc.perform(post("/person/info/save").contentType("application/json").content(jackson.writeValueAsString(preparePerson1Info())).with(SecurityMockMvcRequestPostProcessors.httpBasic("login", "password"))).andExpect(status().isOk());
        mockMvc.perform(post("/person/my-wishes/create").contentType("application/json").content(jackson.writeValueAsString(prepareGift())).with(SecurityMockMvcRequestPostProcessors.httpBasic("login", "password"))).andExpect(status().isOk());
    }

    @Test
    void changeProjectSuccess() throws Exception{
        mockMvc.perform(post("/registration").contentType("application/json").content(jackson.writeValueAsString(preparePerson1Login())));
        mockMvc.perform(post("/person/info/save").contentType("application/json").content(jackson.writeValueAsString(preparePerson1Info())).with(SecurityMockMvcRequestPostProcessors.httpBasic("login", "password"))).andExpect(status().isOk());
        mockMvc.perform(post("/person/project/change").param("project_id", "49ab4b33-2a73-41ef-8bb6-10ea7856a2ad").with(SecurityMockMvcRequestPostProcessors.httpBasic("login", "password"))).andExpect(status().isOk());
    }

    @Test
    void changeProjectNotFound() throws Exception{
        mockMvc.perform(post("/registration").contentType("application/json").content(jackson.writeValueAsString(preparePerson1Login())));
        mockMvc.perform(post("/person/info/save").contentType("application/json").content(jackson.writeValueAsString(preparePerson1Info())).with(SecurityMockMvcRequestPostProcessors.httpBasic("login", "password"))).andExpect(status().isOk());
        mockMvc.perform(post("/person/project/change").param("project_id", "49ab4b33-2a73-41ef-8bb6-10ea7856a2at").with(SecurityMockMvcRequestPostProcessors.httpBasic("login", "password"))).andExpect(status().is4xxClientError());
    }

    @Test
    void giftDoesNotExist() throws Exception{
        mockMvc.perform(post("/registration").contentType("application/json").content(jackson.writeValueAsString(preparePerson1Login())));
        mockMvc.perform(post("/person/info/save").contentType("application/json").content(jackson.writeValueAsString(preparePerson1Info())).with(SecurityMockMvcRequestPostProcessors.httpBasic("login", "password"))).andExpect(status().isOk());
        mockMvc.perform(post("/person/my-wishes/add").param("gift_id", "49ab4b33-2a72-41ef-7ae3-10ea7856a3ed").with(SecurityMockMvcRequestPostProcessors.httpBasic("login", "password"))).andExpect(status().is4xxClientError());
    }

    @Test
    void accountDoesNotExist() throws Exception{
        mockMvc.perform(post("/registration").contentType("application/json").content(jackson.writeValueAsString(preparePerson1Login())));
        mockMvc.perform(post("/person/info/save").contentType("application/json").content(jackson.writeValueAsString(preparePerson1Info())).with(SecurityMockMvcRequestPostProcessors.httpBasic("login", "password"))).andExpect(status().isOk());
        mockMvc.perform(delete("/person/delete").with(SecurityMockMvcRequestPostProcessors.httpBasic("login", "password"))).andExpect(status().isOk());
        mockMvc.perform(post("/person/my-wishes/add").param("gift_id", "49ab4b33-2a72-41ef-7ae3-10ea7856a3ed").with(SecurityMockMvcRequestPostProcessors.httpBasic("login", "password"))).andExpect(status().is4xxClientError());
    }


    private Gift prepareGift(){
        return new Gift("49ab4b33-2a73-42ef-7ae3-10ea7856a3ed", "Lenovo Yoga", "Not that cool but still okay");
    }




}