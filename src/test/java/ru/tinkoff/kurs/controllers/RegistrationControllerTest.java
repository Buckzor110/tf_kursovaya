package ru.tinkoff.kurs.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import ru.tinkoff.kurs.AbstractClass;
import ru.tinkoff.kurs.model.PersonDTO;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class RegistrationControllerTest extends AbstractClass {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper jackson;

    @Test
    void registrationSuccess() throws Exception {
        PersonDTO personDTO = prepareDTO();
        var personDTOJson = jackson.writeValueAsString(personDTO);
        mockMvc.perform(post("/registration").contentType("application/json").content(personDTOJson)).andExpect(status().isOk());
    }

    @Test
    void accountAlreadyExists() throws Exception {
        PersonDTO personDTO = prepareDTO();
        var personDTOJson = jackson.writeValueAsString(personDTO);
        mockMvc.perform(post("/registration").contentType("application/json").content(personDTOJson)).andExpect(status().isOk());
        mockMvc.perform(post("/registration").contentType("application/json").content(personDTOJson)).andExpect(status().is4xxClientError());
    }

    @Test
    void passwordsDoNotMatch() throws Exception {
        PersonDTO personDTO = prepareWrongDTO();
        var personDTOJson = jackson.writeValueAsString(personDTO);
        mockMvc.perform(post("/registration").contentType("application/json").content(personDTOJson)).andExpect(status().is4xxClientError());
    }

    private PersonDTO prepareDTO(){
        return new PersonDTO("login", "password", "password");
    }

    private PersonDTO prepareWrongDTO(){
        return new PersonDTO("login", "password", "password1");
    }

}