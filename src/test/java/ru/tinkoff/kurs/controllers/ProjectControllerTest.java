package ru.tinkoff.kurs.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import ru.tinkoff.kurs.AbstractClass;
import ru.tinkoff.kurs.model.Project;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@TestMethodOrder(MethodOrderer.DisplayName.class)
@WithMockUser(username = "any", password = "thing", roles = {"USER","ADMIN"})
class ProjectControllerTest extends AbstractClass {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper jackson;

//    @Test
//    void deleteNotFound() throws Exception{
//        Project project = anotherProject;
//        mockMvc.perform(delete("/projects/delete").param("id", project.getId())).andExpect(status().is4xxClientError());
//
//    }

    @Test
    void addSuccess() throws Exception{
        Project project = prepareValidProject();
        String projectJSON = jackson.writeValueAsString(project);
        mockMvc.perform(post("/projects/create").contentType("application/json").content(projectJSON)).andExpect(status().isOk());
    }

    @Test
    void projectAlreadyExists() throws Exception{
        Project project = prepareValidProject();
        String projectJSON = jackson.writeValueAsString(project);
        mockMvc.perform(post("/projects/create").contentType("application/json").content(projectJSON)).andExpect(status().isOk());
        mockMvc.perform(post("/projects/create").contentType("application/json").content(projectJSON)).andExpect(status().is4xxClientError());
    }

    @Test
    void invalidEntity() throws Exception{
        Project projectId = prepareInvalidIdProject();
        String projectIdJSON = jackson.writeValueAsString(projectId);
        Project projectName = prepareInvalidNameProject();
        String projectNameJSON = jackson.writeValueAsString(projectId);
        Project projectDescription = prepareInvalidDescriptionProject();
        String projectDescriptionJSON = jackson.writeValueAsString(projectId);
        mockMvc.perform(post("/projects/create").contentType("application/json").content(projectIdJSON)).andExpect(status().is4xxClientError());
        mockMvc.perform(post("/projects/create").contentType("application/json").content(projectNameJSON)).andExpect(status().is4xxClientError());
        mockMvc.perform(post("/projects/create").contentType("application/json").content(projectDescriptionJSON)).andExpect(status().is4xxClientError());
    }

    @Test
    void findProject() throws Exception{
        Project project = prepareValidProject();
        String projectJSON = jackson.writeValueAsString(project);
        mockMvc.perform(post("/projects/create").contentType("application/json").content(projectJSON)).andExpect(status().isOk());
        mockMvc.perform(get("/projects/").param("id", project.getId())).andExpect(status().isOk());
    }

    @Test
    void findProjects() throws Exception{
        Project project = prepareValidProject();
        String projectJSON = jackson.writeValueAsString(project);
        mockMvc.perform(post("/projects/create").contentType("application/json").content(projectJSON)).andExpect(status().isOk());
        mockMvc.perform(get("/projects/all")).andExpect(status().isOk());
    }

    @Test
    void projectNotFound() throws Exception{
        Project project = prepareValidProject();
        String projectJSON = jackson.writeValueAsString(project);
        mockMvc.perform(post("/projects/create").contentType("application/json").content(projectJSON)).andExpect(status().isOk());
        mockMvc.perform(get("/projects/").param("id", "project.getId()")).andExpect(status().is4xxClientError());
    }

    @Test
    void deleteSuccess() throws Exception{
        Project project = prepareValidProject();
        String projectJSON = jackson.writeValueAsString(project);
        mockMvc.perform(post("/projects/create").contentType("application/json").content(projectJSON)).andExpect(status().isOk());
        mockMvc.perform(delete("/projects/delete").param("id", project.getId())).andExpect(status().isOk());

    }

    @Test
    void updateSuccess() throws Exception{
        Project project = prepareValidProject();
        String projectJSON = jackson.writeValueAsString(project);
        Project updatedProject = updateProject();
        String updatedProjectJSON = jackson.writeValueAsString(updatedProject);
        mockMvc.perform(post("/projects/create").contentType("application/json").content(projectJSON)).andExpect(status().isOk());
        mockMvc.perform(patch("/projects/update").contentType("application/json").content(updatedProjectJSON)).andExpect(status().isOk());

    }

    @Test
    void updateNotFound() throws Exception{
        Project project = prepareValidProject();
        String projectJSON = jackson.writeValueAsString(project);
        mockMvc.perform(patch("/projects/delete").contentType("application/json").content(projectJSON)).andExpect(status().is4xxClientError());

    }


    public Project updateProject(){
        return new Project("49ab4b33-2a73-41ef-8bd6-10ea7856a2ad", "Some Updated Project", "Some description");
    }

    public Project prepareValidProject(){
        return new Project("49ab4b33-2a73-41ef-8bd6-10ea7856a2ad", "Some Project", "Some description");
    }
    public Project prepareInvalidIdProject(){
        return new Project("", "Some Project", "Some description");
    }

    public Project prepareInvalidNameProject(){
        return new Project("49ab4b33-2a73-41ef-8bd6-10ea7856a2ad", "", "Some description");
    }
    public Project prepareInvalidDescriptionProject(){
        return new Project("49ab4b33-2a73-41ef-8bd6-10ea7856a2ad", "Some Project", "");
    }

}