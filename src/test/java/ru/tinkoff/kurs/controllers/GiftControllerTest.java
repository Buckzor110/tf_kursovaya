package ru.tinkoff.kurs.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors;
import org.springframework.test.web.servlet.MockMvc;
import ru.tinkoff.kurs.AbstractClass;
import ru.tinkoff.kurs.model.Gift;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@WithMockUser(username = "any", password = "thing", roles = {"USER","ADMIN"})
class GiftControllerTest extends AbstractClass {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper jackson;

    @Test
    void addSuccess() throws Exception{
        Gift gift = prepareValidGift();
        String giftJSON = jackson.writeValueAsString(gift);
        mockMvc.perform(post("/gifts/create").contentType("application/json").content(giftJSON)).andExpect(status().isOk());
    }

    @Test
    void GiftAlreadyExists() throws Exception{
        Gift gift = prepareValidGift();
        String giftJSON = jackson.writeValueAsString(gift);
        mockMvc.perform(post("/gifts/create").contentType("application/json").content(giftJSON)).andExpect(status().isOk());
        mockMvc.perform(post("/gifts/create").contentType("application/json").content(giftJSON)).andExpect(status().is4xxClientError());
    }

    @Test
    void invalidEntity() throws Exception{
        Gift giftId = prepareInvalidIdGift();
        String giftIdJSON = jackson.writeValueAsString(giftId);
        Gift giftName = prepareInvalidNameGift();
        String giftNameJSON = jackson.writeValueAsString(giftName);
        Gift giftDescription = prepareInvalidDescriptionGift();
        String giftDescriptionJSON = jackson.writeValueAsString(giftDescription);
        mockMvc.perform(post("/gifts/create").contentType("application/json").content(giftIdJSON)).andExpect(status().is4xxClientError());
        mockMvc.perform(post("/gifts/create").contentType("application/json").content(giftNameJSON)).andExpect(status().is4xxClientError());
        mockMvc.perform(post("/gifts/create").contentType("application/json").content(giftDescriptionJSON)).andExpect(status().is4xxClientError());
    }

    @Test
    void findGift() throws Exception{
        Gift gift = prepareValidGift();
        String giftJSON = jackson.writeValueAsString(gift);
        mockMvc.perform(post("/gifts/create").contentType("application/json").content(giftJSON)).andExpect(status().isOk());
        mockMvc.perform(get("/gifts/").param("id", gift.getId())).andExpect(status().isOk());
    }

    @Test
    void findGifts() throws Exception{
        Gift Gift = prepareValidGift();
        String giftJSON = jackson.writeValueAsString(Gift);
        mockMvc.perform(post("/gifts/create").contentType("application/json").content(giftJSON)).andExpect(status().isOk());
        mockMvc.perform(get("/gifts/all")).andExpect(status().isOk());
    }

    @Test
    void GiftNotFound() throws Exception{
        Gift gift = prepareValidGift();
        String giftJSON = jackson.writeValueAsString(gift);
        mockMvc.perform(post("/gifts/create").contentType("application/json").content(giftJSON)).andExpect(status().isOk());
        mockMvc.perform(get("/gifts/").param("id", "Gift.getId()")).andExpect(status().is4xxClientError());
    }

    @Test
    void deleteSuccess() throws Exception{
        Gift gift = prepareValidGift();
        String giftJSON = jackson.writeValueAsString(gift);
        mockMvc.perform(post("/gifts/create").contentType("application/json").content(giftJSON)).andExpect(status().isOk());
        mockMvc.perform(delete("/gifts/delete").param("id", gift.getId())).andExpect(status().isOk());

    }

    @Test
    @Order(1)
    void deleteNotFound() throws Exception{
        Gift gift = prepareValidGift();
        String giftJSON = jackson.writeValueAsString(gift);
        mockMvc.perform(delete("/gifts/delete").param("id", gift.getId())).andExpect(status().is4xxClientError());

    }

    @Test
    void updateSuccess() throws Exception{
        Gift gift = prepareValidGift();
        String giftJSON = jackson.writeValueAsString(gift);
        Gift updatedGift = updateGift();
        String updatedGiftJSON = jackson.writeValueAsString(updatedGift);
        mockMvc.perform(post("/gifts/create").contentType("application/json").content(giftJSON)).andExpect(status().isOk());
        mockMvc.perform(patch("/gifts/update").contentType("application/json").content(updatedGiftJSON)).andExpect(status().isOk());

    }

    @Test
    void updateNotFound() throws Exception{
        Gift gift = prepareValidGift();
        String giftJSON = jackson.writeValueAsString(gift);
        mockMvc.perform(patch("/gifts/delete").contentType("application/json").content(giftJSON)).andExpect(status().is4xxClientError());

    }

    @Test
    void getPersonGifts() throws Exception{
        String projectJSON = jackson.writeValueAsString(project);
        String anotherProjectJSON = jackson.writeValueAsString(anotherProject);
        mockMvc.perform(post("/projects/create").contentType("application/json").content(projectJSON)).andExpect(status().isOk());
        mockMvc.perform(post("/projects/create").contentType("application/json").content(anotherProjectJSON)).andExpect(status().isOk());
        for (Gift gift: gifts){
            mockMvc.perform(post("/gifts/create").contentType("application/json").content(jackson.writeValueAsString(gift))).andExpect(status().isOk());
        }
        mockMvc.perform(post("/registration").contentType("application/json").content(jackson.writeValueAsString(preparePerson1Login())));
        mockMvc.perform(post("/registration").contentType("application/json").content(jackson.writeValueAsString(preparePerson2Login())));
        mockMvc.perform(post("/person/info/save").contentType("application/json").content(jackson.writeValueAsString(preparePerson1Info())).with(SecurityMockMvcRequestPostProcessors.httpBasic("login", "password"))).andExpect(status().isOk());
        mockMvc.perform(post("/person/info/save").contentType("application/json").content(jackson.writeValueAsString(preparePerson2Info())).with(SecurityMockMvcRequestPostProcessors.httpBasic("anotherLogin", "password1"))).andExpect(status().isOk());
        mockMvc.perform(post("/person/my-wishes/add").param("gift_id", "49ab4b33-2a73-41ef-7ae3-10ea7856a3ed").with(SecurityMockMvcRequestPostProcessors.httpBasic("login", "password"))).andExpect(status().isOk());
        mockMvc.perform(get("/gifts/person").param("person_id", "49ab4b33-2a73-41ef-7ae3-10ea7856a3ed").with(SecurityMockMvcRequestPostProcessors.httpBasic("login", "password"))).andExpect(status().isOk());
    }


    public Gift updateGift(){
        return new Gift("49ab4b33-2a73-41ef-8bd6-10ea7856a2ad", "Some Updated Gift", "Some description");
    }

    public Gift prepareValidGift(){
        return new Gift("49ab4b33-2a73-41ef-8bd6-10ea7856a2ad", "Some Gift", "Some description");
    }
    public Gift prepareInvalidIdGift(){
        return new Gift("", "Some Gift", "Some description");
    }
    public Gift prepareInvalidNameGift(){
        return new Gift("49ab4b33-2a73-41ef-8bd6-10ea7856a2ad", "", "Some description");
    }
    public Gift prepareInvalidDescriptionGift(){
        return new Gift("49ab4b33-2a73-41ef-8bd6-10ea7856a2ad", "Some Gift", "");
    }

}