package ru.tinkoff.kurs;

import org.junit.jupiter.api.BeforeAll;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.support.TestPropertySourceUtils;
import org.springframework.transaction.annotation.Transactional;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Testcontainers;
import ru.tinkoff.kurs.model.Gift;
import ru.tinkoff.kurs.model.PersonDTO;
import ru.tinkoff.kurs.model.Project;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

@SpringBootTest
@AutoConfigureMockMvc
@Transactional
//@Testcontainers
//@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
//@ContextConfiguration(initializers = AbstractClass.class)
public class AbstractClass{

    static public Project project = new Project("49ab4b33-2a73-41ef-8bd6-10ea7856a2ad", "Some Project", "Some description");

    static public Project anotherProject = new Project("49ab4b33-2a73-41ef-8bb6-10ea7856a2ad", "Some Another Project", "Some Another description");

    static public List<Gift> gifts = List.of(new Gift("49ab4b33-2a73-41ef-8bd6-10ea7856a3ed", "Some Gift", "Gift description"),
                new Gift("49ab4b33-2a73-41ef-7ae3-10ea7856a3ed", "MacBook", "Very cool"),
                new Gift("49ab4b33-2a73-41ef-8bd1-10ea7856a3ed", "HyperX Alloy Origins Core", "Is nice"),
                new Gift("49ab4b33-2a73-41ef-8bd8-10ea7856a3ed", "Razer Viper", "Cool mouse"),
                new Gift("49ab4b33-2a73-41ef-8bd5-10ea7856a3ed", "Behringer C-1", "Solid micro"));


    public static PersonDTO preparePerson1Info(){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        return new PersonDTO("Some Name", "88005553535", "49ab4b33-2a73-41ef-8bd6-10ea7856a2ad", "Ekaterinburg", LocalDate.parse("12-02-2011", formatter), LocalDate.parse("12-02-2011", formatter));
    }

    public static PersonDTO preparePerson2Info(){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        return new PersonDTO("Another Name", "88005553536", "49ab4b33-2a73-41ef-8bd6-10ea7856a2ad", "Ekaterinburg", LocalDate.parse("12-02-2011", formatter), LocalDate.parse("12-02-2011", formatter));
    }

    public static PersonDTO preparePerson1Login(){
        return new PersonDTO("login", "password", "password");
    }

    public static PersonDTO preparePerson2Login(){
        return new PersonDTO("anotherLogin", "password1", "password1");
    }

//    static final PostgreSQLContainer<?> postgres = new PostgreSQLContainer<>("postgres:latest").withReuse(true);
//
//    @BeforeAll
//    public static void setup(){
//        postgres.start();
//    }
//
//    @Override
//    public void initialize(ConfigurableApplicationContext applicationContext) {
//
//        TestPropertySourceUtils.addInlinedPropertiesToEnvironment(
//                applicationContext,
//                "spring.datasource.url ="+ postgres.getJdbcUrl(),
//                "spring.datasource.username ="+ postgres.getUsername(),
//                "spring.datasource.password ="+ postgres.getPassword()
//        );
//    }
}
